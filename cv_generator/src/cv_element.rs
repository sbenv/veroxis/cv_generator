use serde::Deserialize;
use serde::Serialize;

use crate::chapter::Chapter;

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "type", content = "content")]
pub enum CvElement {
    Break,
    Chapter(Chapter),
}

impl CvElement {
    pub fn to_html(&self) -> String {
        match self {
            CvElement::Chapter(chapter) => chapter.to_html(),
            CvElement::Break => String::from("<div style=\"page-break-before:always;\"></div>"),
        }
    }
}
