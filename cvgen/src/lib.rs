use clap::Parser;
use cli::AppArgs;
use commands::convert;
use thiserror::Error;

use crate::commands::init;
use crate::logging::init_logging;

pub mod cli;
pub mod commands;
pub mod filetype;
pub mod logging;

#[derive(Debug, Error)]
pub enum CvGenError {}

pub fn run() -> Result<(), Box<dyn std::error::Error>> {
    init_logging();
    match AppArgs::parse().command {
        cli::Commands::Convert(args) => convert::run(args)?,
        cli::Commands::Init(args) => init::run(args)?,
    };
    Ok(())
}
